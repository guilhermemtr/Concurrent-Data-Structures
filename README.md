# README #

This project is meant to implement concurrent data structures that are so useful for concurrent use, and though there isn't a library that implements most of them.
For this reason, this library intends to implement most known and efficient data structures in order to be as efficient as possible.

### Data Structures to be implemented ###

* Linked List
* HashMap
* RedBlack Tree
* SkipList set

### How it works ###

* Read Write locks
* As fine grained as possible
* Multiple benchmarks