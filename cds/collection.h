#ifndef __CDS_COLLECTION__
#define __CDS_COLLECTION__

#include "iterator.h"
#include "commons.h"

namespace cds {
  template<typename t>
  class collection {
  public:
    virtual void clear() = 0;

    virtual bool add(t& e) = 0;
    virtual bool add_all(collection<t>& s) = 0;
    
    virtual bool remove(t& e) = 0;
    virtual bool remove_all(collection<t>& s) = 0;

    virtual t get(number index) = 0;

    virtual number size() = 0;
    virtual bool is_empty() = 0;
    virtual bool contains(t& e) = 0;
    virtual bool contains_all(collection<t>& s) = 0;

    virtual iterator<t>& beg() = 0;
    virtual iterator<t>& end() = 0;

    /**
     * @param collection of elements to be retained.
     * @return the number of retained elements.
     */
    virtual number retain_all(collection<t>& s) = 0;
    virtual t* elems() = 0;

    virtual ~collection() {};
  };
}

#endif
