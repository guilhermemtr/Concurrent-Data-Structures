#include <iostream>
#include "array_collection.h"
#include "commons.h"

#define NUM_ELEMS 100

using namespace cds;

int
main() {
  array_collection<int> * ac = new array_collection<int>(100);
  int x = 10;
  ac->add(x);
  int g = ac->get(0);

  iterator<int> ac.beg();

  std::cout << g << std::endl;


  return 0;
}
