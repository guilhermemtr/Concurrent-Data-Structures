#ifndef __CDS_COMMONS__
#define __CDS_COMMONS__

#include <stdlib.h>
#include <assert.h>

//include scoped locks and other utils

typedef int number;

#define alloc(...) malloc(__VA_ARGS__)
#define dealloc(...) free(__VA_ARGS__)

#ifdef __CDS_ASSERT__
#define __cds_check(...) assert(__VA_ARGS__)
#else
#define __cds_check(...) while(0)
#endif




#endif