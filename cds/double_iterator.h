#ifndef __CDS_D_ITERATOR__
#define __CDS_D_ITERATOR__

namespace cds {

  template<typename t>
  class double_iterator : public iterator <t> {

  public:
    virtual void operator--() = 0;
    virtual ~double_iterator() {};
  };
}

#endif
