#ifndef __CDS_CONCURRENT_MAP__
#define __CDS_CONCURRENT_MAP__

#include "map.h"

/**
 * cds stands for concurrent data structures
 */
namespace cds {

  template <typename k, typename v>
  class concurrent_map : public map <k,v> {
  public:
    virtual bool put_if_absent(k key, v value) = 0;
    virtual ~concurrent_map() {};
  };
}

#endif
