#ifndef __CDS_LIST__
#define __CDS_LIST__

#include "collection.h"

namespace cds {

  template<typename t>
  class list : public collection<t> {
    public:
      virtual inline bool add_front(t& e) {
        return add(e);
      };

      virtual inline bool add_front_all(collection<t>& s) {
        return add_all(s);
      };

      virtual bool add_back(t& e) = 0;
      virtual bool add_back_all(collection<t>& s) = 0;

      virtual t* get(number index) = 0;

      virtual inline t top() = 0;

      virtual inline t bottom() = 0;

      virtual bool remove_front(t& e) = 0;
      virtual number remove_front_all(number n) = 0;

      virtual inline bool remove_back(t& e) {
        return remove(e);
      };

      virtual number remove_back_all(number n) = 0;

      virtual ~list() {};
  };
}

#endif
