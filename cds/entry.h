#ifndef __CDS_ENTRY__
#define __CDS_ENTRY__

namespace cds {

  template <typename k, typename v>
  class entry {
    k e_key;
    v e_value;
  public:
    inline entry(k& e_key, v& e_value) {
      this->e_key = key;
      this->e_value = e_value;
    }

    inline k key() {
      return this->e_key;
    }

    inline v value() {
      return this->e_value;
    }

    inline ~entry() {}
  };
}

#endif
