#ifndef __CDS_SET__
#define __CDS_SET__

#include "collection.h"

namespace cds {
  // set is just an interface
  template<typename t>
  class set : public collection {};
}

#endif
