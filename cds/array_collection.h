#ifndef __CDS_ARRAY_COLLECTION__
#define __CDS_ARRAY_COLLECTION__

#include "collection.h"

namespace cds {

  template<typename t>
  class array_collection_iterator : public iterator<t> {
    number current_pos;
    collection<t>* c;
  public:

    array_collection_iterator(collection<t>* c, number current_pos = 0) {
      this->current_pos = current_pos;
      this->c = c;
    }

    t operator*() {
      try {
        c->get(current_pos);
      } catch (number index) {
        throw out_of_bounds();
      }
    };

    iterator<t>& operator++() {
      current_pos++;
    };

    iterator<t>& operator++(number v) {
      current_pos++;

    };



    ~array_collection_iterator() {
      c = NULL;
    }
  };

  template<typename t>
  class array_collection : public collection<t> {
    t* arr;
    number ctr;
    number sz;

    public:
      array_collection(number sz = 100) {
        this->sz = sz;
        this->arr = (t*) alloc(this->sz * sizeof(t));
        this->ctr = 0;
      };

      bool add(t& e) {
        if(ctr < sz) arr[ctr++] = e;
        else return false;
        return true;
      };

      bool add_all(collection<t>& e) {
        return true;
      };

      void clear() {
        free(this->arr);
        this->arr = (t*) alloc(this->sz * sizeof(t));
        this->ctr = 0;
      };

      bool contains(t& e) {
        for(number i = 0; i < ctr; i++) {
          if(arr[i] == e) {
            return true;
          }
        }
        return false;
      };

      bool contains_all(collection<t>& s) {
        return false;
      };

      t get(number index) {
        if(index >= 0 && index < ctr)
          return this->arr[index];
        throw index;
      };

      bool is_empty() {
        return this->ctr == 0;
      };

      iterator<t>& beg() {
        return array_collection_iterator<t>(this, 0);
      };

      iterator<t>& end() {
        return array_collection_iterator<t>(this, ctr);
      };

      bool remove(t& e) {
        for(number i = 0; i < sz; i++) {
          if(arr[i] == e) {
            for(number j = i; j < ctr - 1; j++) {
              arr[j] = arr[j + 1];
            }
            ctr--;
            return true;
          }
        }
        return false;
      };

      bool remove_all(collection<t>& s) {
        return false;
      };

      /**
       * @param collection of elements to be retained.
       * @return the number of retained elements.
       */
      number retain_all(collection<t>& s) {
        return 0;
      };

      number size() {
        return this->ctr;
      };

      t* elems() {
        t* ret = (t*) alloc(sz * sizeof(t));
        for(number i = 0; i < sz; i++) {
          ret[i] = arr[i];
        }
        return ret;
      };

      ~array_collection() {
        free(arr);
      };
  };
}

#endif
